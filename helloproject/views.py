from django.shortcuts import render, redirect 
from django.views.generic import View
from django.views.generic import FormView

from helloproject.forms import AboutUsForm
from helloproject.forms import ArticleForm

student_list = [
    {
        "id":"1001",
        "name":"Suju",
        "rollno":"22",
        "address":"patan",
        "mobile":"9874563210",
        "email": "suju@gmail.com"
    },
      {
        "id":"1002",
        "name":"Ram",
        "rollno":"2",
        "address":"lamatar",
        "mobile":"9874662357",
        "email": "ram@gmail.com"
    },
      {
        "id":"1003",
        "name":"Hari",
        "rollno":"13",
        "address":"sundhara",
        "mobile":"9874563147",
        "email": "hari@gmail.com"
    },
      {
        "id":"1004",
        "name":"Rejina",
        "rollno":"17",
        "address":"patan",
        "mobile":"9356563210",
        "email": "rejina@gmail.com"
    },
      {
        "id":"1005",
        "name":"Somi",
        "rollno":"21",
        "address":"chetrapatti",
        "mobile":"9824863210",
        "email": "somi@gmail.com"
    },
      {
        "id":"1006",
        "name":"Sophy",
        "rollno":"20",
        "address":"swotha",
        "mobile":"9835763210",
        "email": "sophy@gmail.com"
    },
      {
        "id":"1007",
        "name":"Ming",
        "rollno":"14",
        "address":"swoyambhu",
        "mobile":"9815963210",
        "email": "ming@gmail.com"
    },
      {
        "id":"1008",
        "name":"Nimmy",
        "rollno":"15",
        "address":"chitwan",
        "mobile":"9872583210",
        "email": "nimmy@gmail.com"
    },
      {
        "id":"1009",
        "name":"Yunika",
        "rollno":"25",
        "address":"chakupat",
        "mobile":"9812363210",
        "email": "yunika@gmail.com"
    },
      {
        "id":"1010",
        "name":"shital",
        "rollno":"19",
        "address":"dang",
        "mobile":"9874563147",
        "email": "shital@gmail.com"
    },
    ]
article_list = [
    {
        "title": "First article",
        "content": "This is the first article",
        "date": "2022-1-2"
    },
    {
        "title": "Second article",
        "content": "This is the Second article",
        "date": "2022-1-3"
    },
    {
        "title": "Third article",
        "content": "This is the Third article",
        "date": "2022-1-4"
    },
    {
        "title": "Fourth article",
        "content": "This is the Fourth article",
        "date": "2022-1-5"
    },
]


def home_view(request):
    context = {
        'name': 'Sujata Bajracahrya',
        'age': 20,
        "address": "Dhapagal",
        "about_job": {
            'company': 'Infodevelopers',
            'location':'sanepa',
        },
        'intrests':['designing', 'programming'],
         'student_list':student_list
    
    }
    return render(request,'home.html', context)

class AboutUsView(FormView):
    form_class = AboutUsForm
    template_name = 'about.html'

    def form_invalid(self, form):
        print('This form is invalid.')
        return super().form_invalid(form)

    def form_valid(self, form):
        print("This is Post Request")
        form = AboutUsForm(self.request.POST)
        if form.is_valid():            
            name = self.request.POST.get('title')
            desc = self.request.POST.get('description')
            print(name,desc, "?????")
        # print(form.errors)
        return render (self.request, 'about.html', {'form': form})
        # return redirect("/")


def about_view(request):
    form = AboutUsForm()
    if request.method == "GET":
        name = request.GET.get('title')
        desc = request.GET.get('description')
        print(name,desc, "?????")
        context = {
            'form': form
            }
        return render (request, 'about.html', context)
    else:
        print("This is Post Request")
        form = AboutUsForm(request.POST)
        if form.is_valid():            
            name = request.POST.get('title')
            desc = request.POST.get('description')
            print(name,desc, "?????")
        # print(form.errors)
        return render (request, 'about.html', {'form': form})
        # return redirect("/")   

def contact_view(request):
    if request.method == "GET":
        name = request.GET.get('email')
        description = request.GET.get('message')
        print(name, description, "?????")
        context = {
        # 'name': 'Sujata Bajracahrya',
        # 'email': 'sujatabajra3333@gmail.com',
         "address": "Dhapagal, Lalitpur",
        # "contact": "9874563210",
         
        }
        return render(request,'contact.html', context)

    else:
        print("This is Post Request")
        name = request.POST.get('email')
        description = request.POST.get('message')
        print(name, description, "?????")
        return redirect("/")
    
class ArticleView(FormView):
    form_class = ArticleForm
    template_name = 'article.html'
    
    def form_invalid(self, form):
        print('This form is invalid.')
        return super().form_invalid(form)

    def form_valid(self, form):
        print("This is Post Request")
        form = ArticleForm(self.request.POST)
        if form.is_valid():            
            title = self.request.POST.get('title')
            content = self.request.POST.get('content')
            print(title ,content, "?????")
            new_article_list = {
            "title": title,
            "content": content
        }
            article_list.append(new_article_list)
            
            
        return render (self.request, 'article.html', {'form': form, 'article_list': article_list})
        # return redirect("/")

def article_view(request):
    form = ArticleForm()
    if request.method == "GET":
        name = request.GET.get('title')
        content = request.GET.get('content')
        print(name,content, "?????")
        context = {
            'form': form,
            'article_list': article_list
            }
        return render(request,'article.html',context)
    else:
        print("This is Post Request")
        form = ArticleForm(request.POST)
        if form.is_valid():            
            title = request.POST.get('title')
            content = request.POST.get('content')
            print(title ,content, "?????")
            new_article_list = {
            "title": title,
            "content": content
        }
            article_list.append(new_article_list)
            
            
        return render (request, 'article.html', {'form': form, 'article_list': article_list})
        # return redirect("/")


expenses= [
                {
                    "title": "Watching Drama",
                     "amount": 60
                },
                {
                    "title": "Travelling",
                    "amount": 1000
                }
            ]
            
        
class ExpenseListView(View):
    def get(self, request):
        context = {
            "expenses": expenses
            
            
        }
        return render (request, 'expense_list.html', context)

    def post(self, request):
        title = request.POST.get("title")
        amount = request.POST.get('amount')
        print("Title:", title)
        print("Amount:", amount)
        new_expense = {
            "title": title,
            "amount": amount
        }
        expenses.append(new_expense)
        return redirect('/expense')