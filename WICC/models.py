from email.policy import default
from django.db import models

# Create your models here.
class AboutUsModel(models.Model):
    title = models.CharField(max_length=255, default='', null=False, blank = False)
    description = models.CharField(max_length =500, null=False, blank=False)

    def __str__(self):
        return self.title