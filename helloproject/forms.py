from django import forms
from django.core.exceptions import ValidationError
TITLES = ['WICC', 'Django', 'Python']
ARTICLES = ['First article', 'Second article', 'Third article', 'Fourth article']
class AboutUsForm(forms.Form):
    title = forms.CharField(max_length=100, required=True, label="Title")
    description = forms.CharField(widget=forms.Textarea)

    def __init__(self, *args, **kwargs):
        super(AboutUsForm, self).__init__(*args, **kwargs)
        
        
        for field in self.fields:
            self.fields[field].widget.attrs.update({'class':'form-control'})

        self.fields['title'].widget.attrs.update({'placeholder':'Enter a title'})
        self.fields['description'].widget.attrs.update({'col': 6})

    def clean(self):
        cleaned_data = super().clean()
        title = self.cleaned_data.get('title')
        description = self.cleaned_data.get('description')
        if title in TITLES:
            raise ValidationError("Sorry, the given title already exists.")
        if '@' in description:
            raise ValidationError("Sorry, @ is not acceptable in the description.")
        return cleaned_data
    
class ArticleForm(forms.Form):
    title = forms.CharField(max_length=100, required=True, label="Title")
    content = forms.CharField(widget=forms.Textarea)

    def __init__(self, *args, **kwargs):
        super(ArticleForm, self).__init__(*args, **kwargs)

        for field in self.fields:
            self.fields[field].widget.attrs.update({'class':'form-control'})
        
        self.fields['title'].widget.attrs.update({'placeholder':'Enter a title'})
        self.fields['content'].widget.attrs.update({'col': 6})

    def clean(self):
        cleaned_data = super().clean()
        title = self.cleaned_data.get('title')
        content = self.cleaned_data.get('content')
        if title in ARTICLES:
            raise ValidationError("Sorry, the given article title already exists.")
        if '@' in content:
            raise ValidationError("Sorry, @ is not acceptable in the content.")
        return cleaned_data