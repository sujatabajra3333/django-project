from django.db import models

# Create your models here.
class ArticleModel(models.Model):
    title = models.CharField(max_length=255, default='', null=False, blank = False)
    content = models.CharField(max_length =500, null=False, blank=False)

   